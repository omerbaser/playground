package com.PlayGround.StepDefinitions;

import com.PlayGround.pages.GamePage;
import com.PlayGround.utilities.BrowserUtils;
import com.PlayGround.utilities.Driver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.Alert;

public class GameStepDefinition {

    GamePage gamePage = new GamePage();

    @Given("the user is on the playground page")
    public void the_user_is_on_the_playground_page() {

        gamePage.openUrl();
    }

    @When("the user clicks all icons in the outer perimeter and the dialog should appear")
    public void the_user_clicks_all_icons_in_the_outer_perimeter_and_the_dialog_should_appear() {

        gamePage.clickIcons();
        gamePage.dialogAppear();
    }

    @And("the user should be able to define size of the grid by entering a valid number")
    public void the_user_should_be_able_to_define_size_of_the_grid_by_entering_a_valid_number() {

        gamePage.enterNumber();

    }

    @And("the user click ok button in pop-up dialog box")
    public void the_user_click_ok_button_in_pop_up_dialog_box() {

        gamePage.clickOkButton();

    }

    @Then("the new grid should open in appropriate number of columns and rows")
    public void the_new_grid_should_open_in_appropriate_number_of_columns_and_rows() {

        gamePage.checkNewGame();

    }

    @When("the user should be able to define size of the grid by entering a invalid number")
    public void the_user_should_be_able_to_define_size_of_the_grid_by_entering_a_invalid_number() {

        gamePage.enterInvalidNumber();

    }

    @Then("display message {string} should appear")
    public void display_message_should_appear(String errorMessage) {

        gamePage.errorMessageAppear();
        Driver.get().close();
    }

    @And("the user enters random characters")
    public void the_user_enters_random_characters() {

        gamePage.enterRandomCharacters();
    }

}
