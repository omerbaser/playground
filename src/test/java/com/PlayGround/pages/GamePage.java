package com.PlayGround.pages;

import com.PlayGround.utilities.BrowserUtils;
import com.PlayGround.utilities.ConfigurationReader;
import com.PlayGround.utilities.Driver;
import io.cucumber.java.en.And;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GamePage extends BasePage{

    public GamePage(){
        PageFactory.initElements(Driver.get(),this);
    }

    public  void openUrl(){
        Driver.get().get(ConfigurationReader.get("url"));
        BrowserUtils.waitFor(2);
        Driver.get().manage().window().maximize();
    }

    public void clickIcons(){

       List<WebElement> allRows = Driver.get().findElements(By.xpath("//div[@class='mainGrid']/div"));

       int [][] gridArray = new int[allRows.size()][allRows.size()] ;


        for (int i = 0; i <allRows.size(); i++){

              String rowXpath = "(//div[@class='mainGrid']/div)[" + (i+1) + "]";

            if (i==0 || i==allRows.size()-1){

                for (int y = 0; y<allRows.size(); y++){
                    String iconsXpath = "(//div[@class='mainGrid']/div)["+ (i+1) +"]/div[@class='icon'][" + (y+1) + "]";
                    WebElement singleIcon = Driver.get().findElement(By.xpath(iconsXpath));
                    singleIcon.click();
                    BrowserUtils.waitFor(1);
                    gridArray[i][y] = y;

                }
            }
            else if (i!=0 || i!=allRows.size()-1){

                for (int y = 0; y<allRows.size(); y++){

                    if (y==0 || y==allRows.size()-1){

                        String iconsXpath = "(//div[@class='mainGrid']/div)["+ (i+1) +"]/div[@class='icon'][" + (y+1) + "]";
                        WebElement singleIcon = Driver.get().findElement(By.xpath(iconsXpath));
                        singleIcon.click();
                    }
                }
            }
        }
    }

    public void dialogAppear(){

        Alert alert = Driver.get().switchTo().alert();
        String dialogBox = alert.getText();
        Assert.assertEquals("Done! Ready for the next try? Give me a size [3-9]:",dialogBox);
    }

    public void enterNumber() {

        Alert alert = Driver.get().switchTo().alert();
        alert.sendKeys("6");
        BrowserUtils.waitFor(1);
    }

    public void clickOkButton(){

        Alert alert = Driver.get().switchTo().alert();
        alert.accept();
    }

    public void checkNewGame(){

        List<WebElement> allRows = Driver.get().findElements(By.xpath("//div[@class='mainGrid']/div"));

        int newGridSize = allRows.size();

        Assert.assertEquals(6, newGridSize);
    }

    public void enterInvalidNumber() {

        Alert alert = Driver.get().switchTo().alert();
        alert.sendKeys("16");
        BrowserUtils.waitFor(1);
    }

    public void errorMessageAppear(){

        Alert alert = Driver.get().switchTo().alert();
        String errorMessage = alert.getText();
        Assert.assertEquals("Not a good size!",errorMessage);
        alert.accept();
    }

    public void enterRandomCharacters() {

        Alert alert = Driver.get().switchTo().alert();
        alert.sendKeys("*&#$@");
        BrowserUtils.waitFor(1);
    }



}
