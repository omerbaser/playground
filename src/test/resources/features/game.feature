@click
Feature: Click cells on the play ground

  @positiveTest
  Scenario: User clicks all icons in the outer perimeter

    Given the user is on the playground page
    When the user clicks all icons in the outer perimeter and the dialog should appear
    And the user should be able to define size of the grid by entering a valid number
    And the user click ok button in pop-up dialog box
    Then the new grid should open in appropriate number of columns and rows

  @boundaryTest
  Scenario: User enters invalid number

    Given the user is on the playground page
    When the user clicks all icons in the outer perimeter and the dialog should appear
    And the user should be able to define size of the grid by entering a invalid number
    And the user click ok button in pop-up dialog box
    Then display message "Not a good size!" should appear

  @negativeTest
  Scenario: User enters random characters

    Given the user is on the playground page
    When the user clicks all icons in the outer perimeter and the dialog should appear
    And the user enters random characters
    And the user click ok button in pop-up dialog box
    Then display message "Not a good size!" should appear









