$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/game.feature");
formatter.feature({
  "name": "Click cells on the play ground",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@click"
    }
  ]
});
formatter.scenario({
  "name": "User enters invalid number",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@click"
    },
    {
      "name": "@boundaryTest"
    }
  ]
});
formatter.step({
  "name": "the user is on the playground page",
  "keyword": "Given "
});
formatter.match({
  "location": "GameStepDefinition.the_user_is_on_the_playground_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user clicks all icons in the outer perimeter and the dialog should appear",
  "keyword": "When "
});
formatter.match({
  "location": "GameStepDefinition.the_user_clicks_all_icons_in_the_outer_perimeter_and_the_dialog_should_appear()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user should be able to define size of the grid by entering a invalid number",
  "keyword": "And "
});
formatter.match({
  "location": "GameStepDefinition.the_user_should_be_able_to_define_size_of_the_grid_by_entering_a_invalid_number()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the user click ok button in pop-up dialog box",
  "keyword": "And "
});
formatter.match({
  "location": "GameStepDefinition.the_user_click_ok_button_in_pop_up_dialog_box()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "display message \"Not a good size!\" should appear",
  "keyword": "Then "
});
formatter.match({
  "location": "GameStepDefinition.display_message_should_appear(String)"
});
formatter.result({
  "status": "passed"
});
});