# MyPlayGround

This is my branch MyPlayGround contains three main scenario with their feature files in Gherkin language and
their StepDefinition files in JAVA language. With these scritps, the all scenario files can be tested/executed (CucumberBDD framework) with POM(Page Object Model) and Singleton.

There is a Pages package that stores locator of elements and common methods to utilize in testing. There is a Runner package to run all tests from one location, I put annotations on the scenarios, tests can be run with unique annotations.

Also There is a utilities classes in the utilities package, they are Driver utility, Browser utility and ConfigurationReader utility. I stored my cucumber feature file separately
and I utilize them in GameStepDefinition in the StepDefinition package, it calls methods from GamePage class.
